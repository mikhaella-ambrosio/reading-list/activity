// JS FUNCTIONS

console.log(`Reading List - JS Functions`);

/*

	Create a function that will accept first name, last name, age, current job, reason for career shift, future goals, and return a complete sentence introducing one's self bearing the input of the user. You may use an object data type as an argument, and access the properties of an object inside the function to get the actual value of properties passed in the function.

*/

function person(fName, lName, age, job, reason, goals){
	return `Hi! I'm ${fName} ${lName} and I am ${age} years old. I'm currenty working as a ${job} and I now want to be a Software Developer. The reason for my career shift is beacuse I want to have ${reason}. I believe that this will help me reach my future goals which are ${goals}.`
};


console.log(
	person (`Mikhaella`, `Ambrosio`, 23, `Electrical Engineering Technologist`, `pandemic proof career`, `fair salary increment and work-life balance`)
);


// IF ELSE STATEMENT 

console.log(`Reading List - If Else Statements`);

/*

	Create a conditional statement that gives a user some options whether they will subscribe to a monthly subscription based on given benefits as a subscriber (you can choose any product/services for this scenario)

*/

console.log(`Welcome to Crunchroll! 
	Please choose your monthly subscription plan: 
	Fan / Mega Fan`);

function subscription(service){
	if (service == `Fan`) {
		return `Monthly subscription: $7.99/mo
		- No Ads 
		- Unlimited access to the Crunchyroll library
		- Access to digital manga
		- Stream on 1 device at a time

		Thank you for choosing Fan subscription! Enjoy!`
	}
	else if (service == `Mega Fan`){
		return `Monthly subscription: $9.99/mo
		- No Ads 
		- Unlimited access to the Crunchyroll library
		- Access to digital manga
		- Stream on 4 device at a time
		- Offiline viewing

		Thank you for choosing Mega Fan subscription! Enjoy!`
	}
};

console.log(`Fan Subscription`);
console.log(subscription(`Fan`));

console.log(`Mega Fan Subscription`);
console.log(subscription(`Mega Fan`));


// TERNARY OPERATOR

/*

	Convert the conditional statement with the use of Ternary Operator

*/


// SWITCH CASE STATEMENT





